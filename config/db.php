<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=yiitest_db_1;port=5432;dbname=yii',
    'username' => 'user',
    'password' => 'user',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
