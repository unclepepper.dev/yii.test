<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class ImageUpload extends Model
{
    public  $image;

    public function rules(): array
    {
        return [
            [['image'], 'required'],
            [['image'], 'file', 'extensions'=>'jpg,png,gif']
        ];
    }

    public function uploadFile(UploadedFile $file, ?string $currentImage)
    {
        $this->image = $file;

        if($this->validate()){

            $this->deleteCurrentImage($currentImage);

            return $this->saveImage($file);

        }
    }

    /**
     * @param string|null $currentImage
     * @return string
     */
    private function getFolder(?string $currentImage): string
    {
        return Yii::getAlias('@web') . 'uploads/' . $currentImage;
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    private function generateFileName(UploadedFile $file): string
    {
        return strtolower(md5(uniqid($file->baseName))) . '.' . $file->extension;
    }

    /**
     * @param string|null $currentImage
     * @return void
     */
    private function deleteCurrentImage(?string $currentImage): void
    {
        if ($this->getFileExists($currentImage)) {

            unlink($this->getFolder($currentImage));

        }
    }

    /**
     * @param string|null $currentImage
     * @return bool
     */
    private function getFileExists(?string $currentImage)
    {
        if(!empty($currentImage)){
            return file_exists($this->getFolder($currentImage));
        }
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    private function saveImage(UploadedFile $file): string
    {
        $filename = $this->generateFileName($file);

        $file->saveAs($this->getFolder($filename));

        return $filename;
    }

    public function deleteImage()
    {
        $imageUploadModel = new ImageUpload();
        
        $imageUploadModel->deleteCurrentImage($this->image);

    }

}